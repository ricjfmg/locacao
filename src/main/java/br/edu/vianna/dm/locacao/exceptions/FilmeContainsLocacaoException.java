/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.exceptions;

/**
 *
 * @author daves
 */
public class FilmeContainsLocacaoException extends Exception {

    public FilmeContainsLocacaoException(String nome) {
        super("O Filme "+nome+" possui locações e não pode ser apagado");
    }
    
}
