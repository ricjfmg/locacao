package br.edu.vianna.dm.locacao.model;

import br.edu.vianna.dm.locacao.util.DataUtils;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Locacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "fk_user", nullable = false)
    private Usuario usuario;

    @ManyToMany(cascade = {
        CascadeType.PERSIST,
        CascadeType.MERGE
    })
    @JoinTable(name = "filmes_locados",
            joinColumns = @JoinColumn(name = "loc_id"),
            inverseJoinColumns = @JoinColumn(name = "filme_id"))
    private List<Filme> filmes;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column(columnDefinition = "DATE")
    private LocalDate dataLocacao;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column(columnDefinition = "DATE")
    private LocalDate dataRetorno;
    
    private boolean devolvido;
    
    private Double valor;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalDate getDataLocacao() {
        return dataLocacao;
    }

    public void setDataLocacao(LocalDate dataLocacao) {
        this.dataLocacao = dataLocacao;
    }

    public LocalDate getDataRetorno() {
        return dataRetorno;
    }

    public void setDataRetorno(LocalDate dataRetorno) {
        this.dataRetorno = dataRetorno;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<Filme> getFilmes() {
        return filmes;
    }

    public void setFilmes(List<Filme> filmes) {
        this.filmes = filmes;
    }

    public boolean isAtrasado(LocalDate hoje) {
        if (DataUtils.ehDomingo(dataLocacao.plusDays(1))) {
            return hoje.isAfter( dataLocacao.plusDays(2)  );
        }else{            
           return hoje.isAfter( dataLocacao.plusDays(1)  );
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isDevolvido() {
        return devolvido;
    }

    public void setDevolvido(boolean devolvido) {
        this.devolvido = devolvido;
    }

}
