/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.util;

import java.time.LocalDate;
import java.time.Month;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author daves
 */
public class DataUtilsTests {
    
    @Test
    public void deveRetornarFalsoParaSabado(){
        //Cenário
        LocalDate sabado = LocalDate.of(2020, Month.AUGUST, 29);
        
        //Execução
        boolean resultado = DataUtils.ehDomingo(sabado);        
        
        //Verificação
        boolean experado = false;
        Assertions.assertEquals(experado, resultado);        
    }
    
    @Test
    public void deveRetornarTrueParaDomingo(){
        //Cenário
        LocalDate sabado = LocalDate.of(2020, Month.AUGUST, 30);
        
        //Execução
        boolean resultado = DataUtils.ehDomingo(sabado);        
        
        //Verificação
        boolean experado = true;
        Assertions.assertEquals(experado, resultado);        
    }
    
    
    @Test
    public void deveRetornarFalsoParaSegunda(){
        //Cenário
        LocalDate sabado = LocalDate.of(2020, Month.AUGUST, 31);
        
        //Execução
        boolean resultado = DataUtils.ehDomingo(sabado);        
        
        //Verificação
        boolean experado = false;
        Assertions.assertEquals(experado, resultado);        
    }
    
}
