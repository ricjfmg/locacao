package br.edu.vianna.dm.locacao.service;

import java.util.List;

import br.edu.vianna.dm.locacao.exceptions.FilmeSemEstoqueException;
import br.edu.vianna.dm.locacao.exceptions.LocadoraException;
import br.edu.vianna.dm.locacao.model.Filme;
import br.edu.vianna.dm.locacao.model.Locacao;
import br.edu.vianna.dm.locacao.model.Usuario;
import br.edu.vianna.dm.locacao.util.DataUtils;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.edu.vianna.dm.locacao.repository.LocacaoDAO;
import br.edu.vianna.dm.locacao.repository.FilmeDAO;
import br.edu.vianna.dm.locacao.repository.UsuarioDAO;

@Service
public class LocacaoService {

    @Autowired
    private LocacaoDAO locDao;
    @Autowired
    private FilmeDAO filmeDao;
    @Autowired
    private UsuarioDAO userDao;

    @Transactional
    public Locacao alugarFilme(Usuario usuario, List<Filme> filmes) throws FilmeSemEstoqueException, LocadoraException {
        if (usuario == null) {
            throw new LocadoraException("Usuario vazio");
        }

        if (filmes == null || filmes.isEmpty()) {
            throw new LocadoraException("Filme vazio");
        }

        for (Filme filme : filmes) {
            if (filme.getEstoque() == 0) {
                throw new FilmeSemEstoqueException();
            }
        }

        Locacao locacao = new Locacao();
        locacao.setFilmes(filmes);
        locacao.setUsuario(usuario);
        locacao.setDataLocacao(LocalDate.now());
        locacao.setDevolvido(false);
        Double valorTotal = 0d;
        for (int i = 0; i < filmes.size(); i++) {
            Filme filme = filmeDao.getOne(filmes.get(i).getId());
            Double valorFilme = filme.getPrecoLocacao();
            switch (i) {
                case 2:
                    valorFilme = valorFilme * 0.75;
                    break;
                case 3:
                    valorFilme = valorFilme * 0.5;
                    break;
                case 4:
                    valorFilme = valorFilme * 0.25;
                    break;
                case 5:
                    valorFilme = 0d;
                    break;
            }
            valorTotal += valorFilme;
            filme.setEstoque(filme.getEstoque() - 1);
            filme = filmeDao.save(filme);
            
        }
        locacao.setValor(valorTotal);

        //Entrega no dia seguinte
        LocalDate dataEntrega = LocalDate.now().plusDays(1);
        if (DataUtils.ehDomingo(dataEntrega)) {
            dataEntrega = dataEntrega.plusDays(1);
        }
        locacao.setDataRetorno(dataEntrega);

        //Salvando a locacao...	
        locacao = locDao.save(locacao);
        return locacao;
    }

    //Fazer TDD
    public double devolverFilme(Locacao loc) throws FilmeSemEstoqueException, LocadoraException {
        if (loc == null) {
            throw new LocadoraException("Locação nula");
        }

        if (loc.getUsuario() == null) {
            throw new LocadoraException("Usuario vazio");
        }

        if (loc.getFilmes() == null || loc.getFilmes().isEmpty()) {
            throw new LocadoraException("Filme vazio");
        }
        double valorPagar = loc.getValor();
        if (loc.isAtrasado(LocalDate.now())) {
            long qtdeDias = DataUtils.diferencaDias(LocalDate.now(), loc.getDataRetorno());
            valorPagar += (3 * qtdeDias);
        }
        //loc.getFilmes().forEach(f -> f.setEstoque(f.getEstoque() + 1));
        for (int i = 0; i < loc.getFilmes().size(); i++) {
            Filme fFilmes = loc.getFilmes().get(i);
            Filme filme = filmeDao.getOne(fFilmes.getId());
            filme.setEstoque(filme.getEstoque() + 1);
            filme = filmeDao.save(filme);
//            loc.getFilmes().set(i, filme);
        }         
        
        loc.setDevolvido(true);
        locDao.save(loc);
        return valorPagar;
    }

    public long locacoesAtivas() {
        return locDao.countByDevolvido(false);
    }

    public boolean existeLocacaoComFilme(Long id) {
        return locDao.countByFilmesId(id) > 0; 
    }

    public List<Locacao> buscarTodos() {
        return locDao.findAll();
    }

    public Locacao buscarPorId(Long id) {
       return locDao.getOne(id);
    }

}
