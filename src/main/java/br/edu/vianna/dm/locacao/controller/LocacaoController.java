package br.edu.vianna.dm.locacao.controller;

import br.edu.vianna.dm.locacao.exceptions.FilmeSemEstoqueException;
import br.edu.vianna.dm.locacao.exceptions.LocadoraException;
import br.edu.vianna.dm.locacao.model.Locacao;
import br.edu.vianna.dm.locacao.service.FilmeService;
import br.edu.vianna.dm.locacao.service.LocacaoService;
import br.edu.vianna.dm.locacao.service.UsuarioService;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/locacao")
public class LocacaoController {

    @Autowired
    private FilmeService filmeService;
    @Autowired
    private UsuarioService userService;
    @Autowired
    private LocacaoService locService;

    @GetMapping("")
    public String locacao(@ModelAttribute("locacao") Locacao locacao, ModelMap model) {

        model.addAttribute("locacoes", locService.buscarTodos());
        model.addAttribute("filmes", filmeService.buscarTodosEmEstoque());
        model.addAttribute("usuarios", userService.buscarTodos());
        return "locacao/cadastro";
    }
    
    @GetMapping("/devolver/{id}")
    public String apagarFilme(@PathVariable("id") Long id, RedirectAttributes attr, ModelMap model) {

	try {
            Locacao loc = locService.buscarPorId(id);
	    locService.devolverFilme(loc);
	} catch (Exception ex) {
	    attr.addFlashAttribute("fail", " " + ex.getMessage());
	    return "redirect:/locacao";
	}
	attr.addFlashAttribute("success", "filme apagado com sucesso.");
	return "redirect:/locacao";
    }
    

    @PostMapping("salvar")
    public String salvarFilme(@Valid Locacao locacao, BindingResult result, RedirectAttributes attr, ModelMap model) {

        if (result.hasErrors()) {
            model.addAttribute("locacoes", locService.buscarTodos());
            model.addAttribute("filmes", filmeService.buscarTodosEmEstoque());
            model.addAttribute("usuarios", userService.buscarTodos());
            model.addAttribute("error", true);
            return "locacao/cadastro";
        }

        try {
            locService.alugarFilme(locacao.getUsuario(), locacao.getFilmes());
        } catch (DataIntegrityViolationException | FilmeSemEstoqueException | LocadoraException e) {
            model.addAttribute("fail", " Erro ao Salvar :: " + e.getMessage());
            model.addAttribute("locacoes", locService.buscarTodos());
            model.addAttribute("filmes", filmeService.buscarTodosEmEstoque());
            model.addAttribute("usuarios", userService.buscarTodos());
            model.addAttribute("error", true);
            return "locacao/cadastro";
        }

        attr.addFlashAttribute("success", " locação realizada com sucesso.");
        return "redirect:/locacao";
    }
}
