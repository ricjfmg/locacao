package br.edu.vianna.dm.locacao.service;

import br.edu.vianna.dm.locacao.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.edu.vianna.dm.locacao.repository.UsuarioDAO;
import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioDAO userDto;

    public List<Usuario> buscarTodos() {
        return userDto.findAll();

    }
    
    public long clientesCadastrados() {
        return userDto.count();
    }

}
