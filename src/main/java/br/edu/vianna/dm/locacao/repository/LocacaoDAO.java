/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.repository;

import br.edu.vianna.dm.locacao.model.Locacao;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author daves
 */
public interface LocacaoDAO extends JpaRepository<Locacao, Long>{

    public Locacao findByUsuarioIdAndDevolvidoFalse(long idUser);
    
    public List<Locacao> findByDevolvido(boolean b);

    public long countByDevolvido(boolean b);

    public int countByFilmesId(Long id);
    
}
