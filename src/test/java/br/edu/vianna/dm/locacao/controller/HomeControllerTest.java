/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.controller;

import br.edu.vianna.dm.locacao.service.FilmeService;
import br.edu.vianna.dm.locacao.service.LocacaoService;
import br.edu.vianna.dm.locacao.service.UsuarioService;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author daves
 */
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@WebMvcTest(HomeController.class)
public class HomeControllerTest {
    
    @Autowired
    private MockMvc mock;
    
    @MockBean
    private LocacaoService s;    
    @MockBean
    private FilmeService f;
    @MockBean
    private UsuarioService u;
    
    @Test
    public void deveAcessarAHome() throws Exception{
        
        Mockito.when(f.filmesDisponiveis() ).thenReturn( (long)10 );
        Mockito.when(u.clientesCadastrados() ).thenReturn( (long)10000 );
        Mockito.when(s.locacoesAtivas() ).thenReturn( (long)100 );
        
        //execução
        ResultActions ra = mock.perform(get("/home"));
        
        //verificação
        ra.andExpect( status().isOk() ).andDo(print());
        
    }
    
    @Test
    public void deveAcessarABarra() throws Exception{
        
        Mockito.when(f.filmesDisponiveis() ).thenReturn( (long)10 );
        Mockito.when(u.clientesCadastrados() ).thenReturn( (long)10000 );
        Mockito.when(s.locacoesAtivas() ).thenReturn( (long)100 );
        
        //execução
        ResultActions ra = mock.perform(get("/"));
        
        //verificação
        ra.andExpect( status().isOk() )
                .andExpect( view().name("home") )
                .andDo(print());
        
    }
    
    @Test
    public void deveRetornar404PAginaNaoExist() throws Exception{
               
        //execução
        ResultActions ra = mock.perform(get("/abc"));
        
        //verificação
        ra.andExpect( status().isNotFound() )
                .andDo(print());
        
    }
    
    
    
}
