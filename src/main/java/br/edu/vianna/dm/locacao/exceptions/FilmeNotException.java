/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.exceptions;

/**
 *
 * @author daves
 */
public class FilmeNotException extends Exception {

    public FilmeNotException() {
        super("O Filme não encontrado");
    }
    
}
