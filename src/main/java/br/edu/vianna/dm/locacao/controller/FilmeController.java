package br.edu.vianna.dm.locacao.controller;

import br.edu.vianna.dm.locacao.model.Filme;
import br.edu.vianna.dm.locacao.service.FilmeService;
import java.util.Optional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/filme")
public class FilmeController {

    @Autowired
    private FilmeService filmeService;

    @GetMapping("")
    public String filme(@ModelAttribute("filme") Filme filme, ModelMap model) {

	model.addAttribute("filmes", filmeService.buscarTodos());
	return "filme/cadastro";
    }
    
    @GetMapping("/excluir/{id}")
    public String apagarFilme(@PathVariable("id") Long id, RedirectAttributes attr, ModelMap model) {

	try {
	    filmeService.apagar(id);
	} catch (Exception ex) {
	    attr.addFlashAttribute("fail", " " + ex.getMessage());
	    return "redirect:/filme";
	}
	attr.addFlashAttribute("success", "filme apagado com sucesso.");
	return "redirect:/filme";
    }

    
    @PostMapping("salvar")
    public String salvarFilme(@Valid Filme filme, BindingResult result, RedirectAttributes attr, ModelMap model) {

	if (result.hasErrors()) {
	    model.addAttribute("filmes", filmeService.buscarTodos());
	    model.addAttribute("error", true);
	    return "filme/cadastro";
	}

	try {
	    filmeService.salvar(filme);
	} catch (DataIntegrityViolationException e) {
	    model.addAttribute("fail", " Erro ao Salvar :: " + e.getMessage());
	    model.addAttribute("filmes", filmeService.buscarTodos());
	    model.addAttribute("error", true);
	    return "filme/cadastro";
	}

	attr.addFlashAttribute("success", filme.getNome() + " inserido com sucesso.");
	return "redirect:/filme";
    }

    @PostMapping("editar")
    public String editarFilme(@Valid Filme filme, BindingResult result, RedirectAttributes attr,
	    ModelMap model) {

	if (result.hasErrors()) {
	    model.addAttribute("filmes", filmeService.buscarTodos());
	    model.addAttribute("error", true);
	    return "filme/cadastro";
	}

	try {
	    filmeService.salvar(filme);
	} catch (DataIntegrityViolationException e) {
	    model.addAttribute("fail", " Erro ao Salvar :: " + e.getMessage());
	    model.addAttribute("filmes", filmeService.buscarTodos());
	    model.addAttribute("error", true);
	    return "filme/cadastro";
	}

	attr.addFlashAttribute("success", filme.getNome() + " inserido com sucesso.");
	return "redirect:/filme";
    }

    @GetMapping("/editar/{id}")
    public String preEditar(@PathVariable("id") long id, ModelMap model, RedirectAttributes attr) {

	Optional<Filme> filme = filmeService.buscarPorId(id);
	model.addAttribute("filmes", filmeService.buscarTodos());

	if (filme.isPresent()) {
	    model.addAttribute("filme", filme.get());
	    model.addAttribute("error", true);
	    return "filme/cadastro";
	} else {
	    model.addAttribute("coleta", new Filme());
	    attr.addFlashAttribute("fail", "filme não Encontrado :: ");
	    return "redirect:/filme";
	}

    }

}
