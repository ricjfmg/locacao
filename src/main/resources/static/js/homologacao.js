/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {


    if (window.location.hostname.includes('integra-h.nrc')) {
        $('body').css('background-color', '#b2c2bf');
        showNotificationBar("Este é um Ambiente de Homologação, nada feito aqui tem validade!!!");
    }

});


function showNotificationBar(message, duration, bgColor, txtColor, height) {

    /*set default values*/
    duration = typeof duration !== 'undefined' ? duration : 1500;
    bgColor = typeof bgColor !== 'undefined' ? bgColor : "#F4E0E1";
    txtColor = typeof txtColor !== 'undefined' ? txtColor : "#A42732";
    height = typeof height !== 'undefined' ? height : 30;
    /*create the notification bar div if it doesn't exist*/
    if ($('#notification-bar').length == 0) {
        var HTMLmessage = "<div class='notification-message' style='text-align:center; line-height: " + height + "px;'> " + message + " </div>";
        $('body').prepend("<div id='notification-bar' style='display:none; width:100%; height:" + height + "px; background-color: " + bgColor + "; position: fixed; z-index: 100; color: " + txtColor + ";border-bottom: 1px solid " + txtColor + ";'>" + HTMLmessage + "</div>");
    }
    /*animate the bar*/
    $('#notification-bar').slideDown()
//        $('#notification-bar').slideDown(function () {
//            setTimeout(function () {
//                $('#notification-bar').slideUp(function () {});
//            }, duration);
//        });
}
