package br.edu.vianna.dm.locacao.controller;

import br.edu.vianna.dm.locacao.service.FilmeService;
import br.edu.vianna.dm.locacao.service.LocacaoService;
import br.edu.vianna.dm.locacao.service.UsuarioService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class HomeController {

    
    @Autowired
    private LocacaoService serv;    
    @Autowired
    private FilmeService film;
    @Autowired
    private UsuarioService user;

    @GetMapping(path = {"/", "/home"})
    public String home(ModelMap model) {

        model.addAttribute("filmesDisponiveis", film.filmesDisponiveis());
        model.addAttribute("clientesCadastrados", user.clientesCadastrados());
        model.addAttribute("locacoesAtivas", serv.locacoesAtivas());

        return "home";
    }


}
