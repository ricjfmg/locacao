package br.edu.vianna.dm.locacao.util;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class DataUtils {

    public static boolean ehDomingo(LocalDate dataEntrega) {
    
        return dataEntrega.getDayOfWeek() == DayOfWeek.SUNDAY;
    
    }

    public static long diferencaDias(LocalDate hoje, LocalDate dataRetorno) {
        return ChronoUnit.DAYS.between(dataRetorno, hoje );
    }
	
}
