/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.repository;

import br.edu.vianna.dm.locacao.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author daves
 */
public interface UsuarioDAO extends JpaRepository<Usuario, Long>{
    
}
