/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.service;

import br.edu.vianna.dm.locacao.exceptions.FilmeSemEstoqueException;
import br.edu.vianna.dm.locacao.exceptions.LocadoraException;
import br.edu.vianna.dm.locacao.model.Filme;
import br.edu.vianna.dm.locacao.model.Locacao;
import br.edu.vianna.dm.locacao.model.Usuario;
import br.edu.vianna.dm.locacao.repository.FilmeDAO;
import br.edu.vianna.dm.locacao.repository.LocacaoDAO;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.assertj.core.api.Assertions;
import static org.assertj.core.api.Assertions.fail;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author daves
 */
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class LocacaoServiceTest {
    

    @MockBean
    private LocacaoDAO locDao;
    @MockBean
    private FilmeDAO filmeDao;   
    
    @InjectMocks
    private LocacaoService locServ;
    
    @BeforeEach
    public void init(){
       MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void deveConseguiAlugar1Filme() throws FilmeSemEstoqueException, LocadoraException{
        //cenário
        Usuario user = new Usuario("Zezin");
        List<Filme> filmes = new ArrayList<Filme>();
        filmes.add( new Filme(2l,"Poeira em Alto Mar", 1, 5.0) );
            //Definir o comportamento das mocks
            Mockito.when(filmeDao.getOne(2l)).thenReturn(filmes.get(0) );
            Filme f = new Filme(2l,"Poeira em Alto Mar", 1, 5.0);
            f.setEstoque(0);
            Mockito.when(filmeDao.save(Mockito.any(Filme.class))).thenReturn( f  ); 
            Mockito.when(locDao.save( Mockito.any(Locacao.class) )).thenAnswer(i -> i.getArguments()[0]);            
        
        //Execução
        Locacao loc = locServ.alugarFilme(user, filmes);
        
        //Verificção
        Assertions.assertThat(loc.getFilmes().get(0).getEstoque())
                .isEqualTo(0);
        
    }
    
   
    @Test
    @DisplayName(" Deve retornar uma exceção qdo passar uma locacao sem filme ")
    public void deveRetornarErroAoDevolverLocacaoSemFilme(){
        //cenário
        Usuario user = new Usuario("Zezin");
        List<Filme> filmes = new ArrayList<Filme>();
        filmes.add( new Filme(2l,"Poeira em Alto Mar", 1, 5.0) );
        
        Locacao loc = new Locacao();
        loc.setValor(15.0);
        loc.setDataLocacao(LocalDate.now());
        loc.setDataRetorno(LocalDate.now().plusDays(1));
        loc.setDevolvido(false);
        loc.setFilmes(null );
        loc.setUsuario(user);
        loc.setId(1);        
        
        try {
            //Execução
            double valor = locServ.devolverFilme(loc);
            fail("não deveria realizar a devolução");
            // org.junit.jupiter.api.Assertions.assertTrue(false);
        } catch (LocadoraException ex) {
            org.junit.jupiter.api.Assertions.assertTrue(true);
        } catch (FilmeSemEstoqueException ex) {
            Logger.getLogger(LocacaoServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            //fail("não deveria realizar a devolução");
         
         //Verificção
//         Assertions.assertThat(loc.getValor())
//                .isEqualTo(10);
    }

    
    @Test
    @DisplayName(" Deve retornar uma exceção qdo passar uma locacao sem filme ")
    public void deveDevolverLovacao() throws LocadoraException, FilmeSemEstoqueException{
        //cenário
        Usuario user = new Usuario("Zezin");
        List<Filme> filmes = new ArrayList<Filme>();
        filmes.add( new Filme(2l,"Poeira em Alto Mar", 1, 5.0) );
        
        Locacao loc = new Locacao();
        loc.setValor(15.0);
        loc.setDataLocacao(LocalDate.now());
        loc.setDataRetorno(LocalDate.now().plusDays(1));
        loc.setDevolvido(false);
        loc.setFilmes(filmes );
        loc.setUsuario(user);
        loc.setId(1);
        
        
        Mockito.when(filmeDao.save(Mockito.any(Filme.class))).thenReturn(loc.getFilmes().get(0)  ); 
        Mockito.when(filmeDao.getOne(2l)).thenReturn(loc.getFilmes().get(0) );
     
        
        //Execução
        double valor = locServ.devolverFilme(loc);
                 
         //Verificção
         Assertions.assertThat(valor)
                .isEqualTo(15);
         Assertions.assertThat(loc.getFilmes().get(0).getEstoque() )
                .isEqualTo(2);
    }
    
    //public void deveConseguiAlugar2Filme()

    //public void alugarFilmesSemEstoque()
            
            
    
}
