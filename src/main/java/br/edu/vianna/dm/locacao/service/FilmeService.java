package br.edu.vianna.dm.locacao.service;

import br.edu.vianna.dm.locacao.exceptions.FilmeContainsLocacaoException;
import br.edu.vianna.dm.locacao.exceptions.FilmeNotException;
import br.edu.vianna.dm.locacao.model.Filme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.edu.vianna.dm.locacao.repository.FilmeDAO;
import java.util.List;
import java.util.Optional;

@Service
public class FilmeService {

    @Autowired
    private FilmeDAO filmeDto;

    @Autowired
    private LocacaoService locServ;

    public long filmesDisponiveis() {
        return filmeDto.count();
    }

    public List<Filme> buscarTodos() {
        return filmeDto.findAll();

    }

    public void salvar(Filme filme) {
        filmeDto.save(filme);
    }

    public void apagar(Long id) throws FilmeNotException, FilmeContainsLocacaoException {
        Optional<Filme> req = filmeDto.findById(id);
        if (req.isPresent()) {
            Filme u = req.get();
            if (!locServ.existeLocacaoComFilme(u.getId())) {
                filmeDto.delete(u);
            } else {
                throw new FilmeContainsLocacaoException(u.getNome());
            }
        } else {
            throw new FilmeNotException();
        }

    }

    public Optional<Filme> buscarPorId(long id) {
        return filmeDto.findById(id);
    }

    public List<Filme> buscarTodosEmEstoque() {
        return filmeDto.findByEstoqueGreaterThan(0);
    }

}
