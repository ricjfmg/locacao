/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.model;

import java.time.LocalDate;
import java.time.Month;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author daves
 */
public class LocacaoTest {
    
    @Test
    public void testSomeMethod() {
        //cenario
        Locacao loc = new Locacao();        
        loc.setDataLocacao(LocalDate.of(2020, Month.AUGUST, 29));
        //execução
        boolean resultado = loc.isAtrasado(LocalDate.of(2020, Month.AUGUST, 31));
        
        //verificação
         Assertions.assertThat(resultado).isFalse();
    }
    
    @Test
    public void testAtrasado() {
        //cenario
        Locacao loc = new Locacao();        
        loc.setDataLocacao(LocalDate.of(2020, Month.AUGUST, 29));
        //execução
        boolean resultado = loc.isAtrasado(LocalDate.of(2020, Month.SEPTEMBER, 01));
        
        //verificação
         Assertions.assertThat(resultado).isTrue();
    }
    
    @Test
    public void testNormal(){
        //cenario
        Locacao loc = new Locacao();        
        loc.setDataLocacao(LocalDate.of(2020, Month.AUGUST, 25));
        //execução
        boolean resultado = loc.isAtrasado(LocalDate.of(2020, Month.AUGUST, 26));
        
        //verificação
         Assertions.assertThat(resultado).isFalse();
    }
    
    @Test
    public void testMuitoAtrasado(){
        //cenario
        Locacao loc = new Locacao();        
        loc.setDataLocacao(LocalDate.of(2020, Month.AUGUST, 25));
        //execução
        boolean resultado = loc.isAtrasado(LocalDate.of(2020, Month.SEPTEMBER, 26));
        
        //verificação
         Assertions.assertThat(resultado).isTrue();
    }
    
}
