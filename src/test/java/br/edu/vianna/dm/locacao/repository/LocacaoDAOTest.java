/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.repository;

import br.edu.vianna.dm.locacao.model.Filme;
import br.edu.vianna.dm.locacao.model.Locacao;
import br.edu.vianna.dm.locacao.model.Usuario;
import java.time.LocalDate;
import java.util.ArrayList;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author daves
 */
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
public class LocacaoDAOTest {
    
    @Autowired
    private UsuarioDAO userD;
    
    @Autowired
    private FilmeDAO filmeD;
    
    @Autowired
    private LocacaoDAO locD;
    
    private Usuario u1,u2,u3;
    private Filme f1,f2;
    private Locacao loc1,loc2; 
    
    @BeforeEach
    public void init(){
        System.out.println("Inserindo");
        u1 = new Usuario(0, "Zezin");
        u2 = new Usuario(0, "Pedrin");
        u3 = new Usuario(0, "Gustin");
        userD.save( u1 );
        userD.save( u2 );
        userD.save( u3 );
        f1 = new Filme("Quina da mesa redonda", 1, 5.0);
        f2 = new Filme("Trança dos Carecas", 0, 7.0);
        filmeD.save(f1);
        filmeD.save(f2);
        
        loc1 = new Locacao();
        loc1.setDataLocacao(LocalDate.now());
        loc1.setDataRetorno(LocalDate.now().plusDays(1));
        loc1.setDevolvido(false);
        ArrayList<Filme> filmes = new ArrayList<>();
        filmes.add(f1);
        loc1.setFilmes(filmes);
        loc1.setUsuario(u1);
        loc1.setValor(5.0);
        locD.save(loc1);
        
        loc2 = new Locacao();
        loc2.setDataLocacao(LocalDate.now().minusDays(3));
        loc2.setDataRetorno(LocalDate.now());
        loc2.setDevolvido(true);
         filmes = new ArrayList<>();
        filmes.add(f1);
        filmes.add(f2);
        loc2.setFilmes(filmes);
        loc2.setUsuario(u2);
        loc2.setValor(12.0);
        locD.save(loc2);
        
        
    }
    
    @AfterEach
    public void clean(){
        System.out.println("apagando");
        locD.deleteAll();
        filmeD.deleteAll();
        userD.deleteAll();
    }
    
    @Test
    public void verificaQuantidadeLocacoesDeveolvidas(){
        //Cenário
        
        //Execução
        long qtdeDevolvidas = locD.countByDevolvido(true);
        
        //Verificacao
        Assertions.assertThat(qtdeDevolvidas).isEqualTo(1);
        
    }
    
    @Test
    public void verificaQuantidadeLocacoes(){
        //cenário
        //Cenário já foi montado
        
        //execução
        long qtdeUsuario = userD.count();
        
        //Verificação
        Assertions.assertThat(qtdeUsuario).isEqualTo(3);
    }
    
    
}
