/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.repository;

import br.edu.vianna.dm.locacao.model.Filme;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author daves
 */
public interface FilmeDAO extends JpaRepository<Filme, Long>{

    public List<Filme> findByEstoqueGreaterThan(int i);
    
}
