/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.runner;

import br.edu.vianna.dm.locacao.model.Filme;
import br.edu.vianna.dm.locacao.model.Usuario;
import br.edu.vianna.dm.locacao.repository.FilmeDAO;
import br.edu.vianna.dm.locacao.repository.UsuarioDAO;
import br.edu.vianna.dm.locacao.service.LocacaoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author daves
 */
@Slf4j
@Component
@Profile("dev")
@Order(1)
public class DataBaseRunner implements ApplicationRunner {

//    private static final log log = logFactory.getlog(this);
//    private final log log = logFactory.getlog(this.getClass());
    @Autowired
    private Environment env;

    @Autowired
    private UsuarioDAO user;
    @Autowired
    private FilmeDAO filme;
    @Autowired
    private LocacaoService serv;

    @Transactional
    @Override
    public void run(ApplicationArguments args) throws Exception {

        Usuario u1, u2, u3 = null;
        Filme f1, f2, f3, f4;
        if (user.count() == 0) {
            u1 = user.save(new Usuario("Zezin"));
            u2 = user.save(new Usuario("Pedrin"));
            u3 = user.save(new Usuario("Gustin"));

            f1 = filme.save(new Filme("Poeira em Alto Mar", 2, 5.0));
            f2 = filme.save(new Filme("A Volta dos que não foram", 1, 9.0));
            f3 = filme.save(new Filme("Quina da Mesa Redonda", 3, 5.0));
            f4 = filme.save(new Filme("O Poeta Mudo", 1, 10.0));
            List<Filme> lista = new ArrayList<>();
            lista.add(f4);
            serv.alugarFilme(u3, lista);
        }

    }

}
